package com.eugene.sharedpreferences;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignUpActivity extends AppCompatActivity {

    private String TAG = "InputTAG";

    private EditText editTextUsername;
    private EditText editTextPassword;
    private EditText editTextConfirmPassword;
    private Button buttonSignIn;
    private Button buttonSignUp;

    private SharedPreferencesService sharedPreferencesService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        sharedPreferencesService = new SharedPreferencesService(getApplicationContext());
        initViews();
        initListeners();
    }

    private void initListeners() {
        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "SignUpActivity -> buttonSignIn clicked!");
                onBackPressed();
            }
        });

        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "SignUpActivity -> buttonSignUp clicked!");

                RegisterUser registerUser = getInputUser();

                if(isRegister(registerUser)) {
                    sharedPreferencesService.saveUser(User.convert(registerUser));
                    showToast("Your registered!");
                } else {
                    showToast("Your not registered!");
                }
            }
        });
    }

    private boolean isRegister(RegisterUser registerUser) {
        return registerUser.isValid() &&
                !sharedPreferencesService.userIsRegistered(registerUser.getUsername());
    }

    private void showToast(String message) {
        Toast.makeText(
                getApplicationContext(),
                message,
                Toast.LENGTH_SHORT).show();
    }

    private RegisterUser getInputUser() {
        return new RegisterUser(
                editTextUsername.getText().toString(),
                editTextPassword.getText().toString(),
                editTextConfirmPassword.getText().toString()
        );
    }

    private void initViews() {
        editTextUsername = findViewById(R.id.editTextUsername);
        editTextPassword = findViewById(R.id.editTextPassword);
        editTextConfirmPassword = findViewById(R.id.editTextConfirmPassword);
        buttonSignIn = findViewById(R.id.buttonSignIn);
        buttonSignUp = findViewById(R.id.buttonSignUp);
    }
}
