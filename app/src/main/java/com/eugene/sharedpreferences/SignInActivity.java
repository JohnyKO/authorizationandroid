package com.eugene.sharedpreferences;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignInActivity extends AppCompatActivity {

    private String TAG = "InputTAG";

    private EditText editTextUsername;
    private EditText editTextPassword;
    private Button buttonSignIn;
    private Button buttonSignUp;

    private SharedPreferencesService sharedPreferencesService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        sharedPreferencesService = new SharedPreferencesService(getApplicationContext());
        initViews();
        initListeners();
    }

    private void initListeners() {
        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "SignInActivity -> buttonSignIn clicked!");
                User user = getInputUser();
                if(sharedPreferencesService.checkUser(user)) {
                    //TODO add new Activity Welcome
                    showToast("Your authorized!");
                } else {
                    showToast("Your not authorized!");
                }
            }
        });

        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "SignInActivity -> buttonSignUp clicked!");
                startActivitySignUp();
            }
        });
    }

    private void showToast(String message) {
        Toast.makeText(
                getApplicationContext(),
                message,
                Toast.LENGTH_SHORT).show();
    }

    private void startActivitySignUp() {
        Intent signUpActivityIntent = new Intent(
                this,
                SignUpActivity.class);
        startActivity(signUpActivityIntent);
    }

    private User getInputUser() {
        return new User(
                editTextUsername.getText().toString(),
                editTextPassword.getText().toString());
    }

    private void initViews() {
        editTextUsername = findViewById(R.id.editTextUsername);
        editTextPassword = findViewById(R.id.editTextPassword);
        buttonSignIn = findViewById(R.id.buttonSignIn);
        buttonSignUp = findViewById(R.id.buttonSignUp);
    }
}
