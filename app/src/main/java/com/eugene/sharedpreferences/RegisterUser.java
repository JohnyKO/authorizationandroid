package com.eugene.sharedpreferences;

public class RegisterUser {
    private String username;
    private String password;
    private String confirmPassword;

    public RegisterUser() {
    }

    public RegisterUser(String username, String password, String confirmPassword) {
        setUsername(username);
        setPassword(password);
        setConfirmPassword(confirmPassword);
    }

    public String getUsername() {
        return username;
    }

    public boolean isValid() {
        if(username.equals(""))
            return false;
        if(password.equals(""))
            return false;
        if(!password.equals(confirmPassword))
            return false;
        return true;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
