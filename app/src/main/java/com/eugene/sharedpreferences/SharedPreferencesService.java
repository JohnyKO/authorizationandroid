package com.eugene.sharedpreferences;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesService {
    private final String fileName = "MuTestFile";

    private SharedPreferences preferences;

    public SharedPreferencesService(Context context) {
        preferences = context.getSharedPreferences(fileName, Context.MODE_PRIVATE);
    }

    @SuppressLint("CommitPrefEdits")
    public boolean saveUser(User user) {
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(user.getUsername(), user.getPassword());
        return editor.commit();
    }

    public boolean userIsRegistered(String username) {
        String innerPassword = preferences.getString(username, "");
        return !innerPassword.equals("");
    }

    public boolean checkUser(User user) {
        String innerPassword = preferences.getString(user.getUsername(), "");
        return innerPassword.equals(user.getPassword());
    }
}
